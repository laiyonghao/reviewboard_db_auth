# REVIEWBOARD DATABASE AUTHORIZATION BACKEND

This module only has tested on 

 - Ubuntu 10.04 LTS
 - OS X 10.6.x


## Installation and Configuration on Ubuntu

install required packages

    sudo apt-get install apache2
    sudo easy_install ReviewBoard


create a ReviewBoard instance and run it in Apache HTTPD + mod_wsgi,  
http://www.reviewboard.org/docs/manual/1.6/admin/installation/creating-sites/#creating-sites


install this modules

    python setup.py install


WARNING:  DO NOT install it via `python setup.py develop`!


re-start Apache HTTPD process, on Ubuntu

    sudo /etc/init.d/apache2 reload


Login in as admin,
Settings -> Authentication -> Authentication Method,
select 'Database Authentication',  
setup required fields in 'Database Authorization Backend Settings',  
click "save".


## Installation and Configuration on OS X

install required packages

    sudo port install apache2 python27
    sudo port install mod_wsgi +python27
    sudo easy_install ReviewBoard


create a ReviewBoard instance and run it in Apache HTTPD + mod_wsgi,  
http://www.reviewboard.org/docs/manual/1.6/admin/installation/creating-sites/#creating-sites


install this modules

    python setup.py install


re-start Apache HTTPD process, on OS X

    sudo port unload apache2
    sudo port load apache2


Login in as admin,  
Settings -> Authentication -> Authentication Method,  
setup required fields in 'Database Authorization Backend Settings',  
click "save".


## REFERENCES

 - http://www.reviewboard.org/docs/codebase/dev/extending/auth-backends/
 - https://docs.djangoproject.com/en/1.3/topics/auth/
